import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Video } from '../class/video';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  url = "http://localhost:8000";

  constructor(private http: HttpClient) { }

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  // GET /history
  getHistory(): Observable<Video[]> {
    return this.http.get<Video[]>(this.url + "/history");
  }

  // GET /bookmark
  getBookmarks(): Observable<Video[]> {
    return this.http.get<Video[]>(this.url + "/bookmark");
  }

  // POST /history
  addHistory(video: Video): Observable<Video> {
    return this.http.post<Video>(this.url + "/history", video, this.httpOptions);
  }

  // POST /bookmark
  addBookmark(video: Video): Observable<Video> {
    return this.http.post<Video>(this.url + "/bookmark", video, this.httpOptions);
  }

}
