import { Component, OnInit } from '@angular/core';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { Video } from './class/video';
import { ApiService } from './services/api-service.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  title: string = 'ReliefApps Test';
  hSnackPosition: MatSnackBarHorizontalPosition = "center";
  vSnackPosition: MatSnackBarVerticalPosition = "top";
  
  currentVideo: Video = new Video();
  currentVideoBookmark: boolean = false;
  
  history: Video[] = [];
  bookmarks: Video[] = [];

  constructor(public api: ApiService, private _snackBar: MatSnackBar) { }
  
  ngOnInit(): void {
    setTimeout(() => {
      this.loadHistory();
    }, 1500);
    this.loadBookMarks();
  }

  openSnackBar(message: string) {
    this._snackBar.open(message, "Close", {
      duration: 5000,
      horizontalPosition: this.hSnackPosition,
      verticalPosition: this.vSnackPosition
    });
  }

  loadHistory() {
    return this.api.getHistory().subscribe((data: Video[]) => {
      this.openSnackBar(`Saved ${data.length} videos in History`);
      this.history = data;
    });
  }

  loadBookMarks() {
    return this.api.getBookmarks().subscribe((data: Video[]) => {
      this.openSnackBar(`Saved ${data.length} videos in Bookmark`);
      this.bookmarks = data;
    });
  }

  onSearchVideo(id: string) {
    this.currentVideo.id = id;
    
    this.api.getBookmarks().subscribe((data: Video[]) => {
      let isBook = false;
      data.forEach((val) => {
        if (val.id == id)
          isBook = true
      });
      this.currentVideoBookmark = isBook;
    })

    this.api.addHistory(this.currentVideo).subscribe(data => {
      this.loadHistory();
    });
  }

  onBookVideo(book: boolean) {
    this.api.addBookmark(this.currentVideo).subscribe(data => {
      this.loadBookMarks();
      this.currentVideoBookmark = book;
    });
  }

  onSelectedVideo(video: Video) {
    this.currentVideo = video;
    this.api.getBookmarks().subscribe((data: Video[]) => {
      let isBook = false;
      data.forEach((val) => {
        if (val.id == video.id)
          isBook = true;
      });
      this.currentVideoBookmark = isBook;
    });
  }

}
