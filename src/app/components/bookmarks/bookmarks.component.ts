import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Video } from 'src/app/class/video';

@Component({
  selector: 'Bookmarks',
  templateUrl: './bookmarks.component.html',
  styleUrls: ['./bookmarks.component.css']
})
export class BookmarksComponent implements OnInit {

  @Input() bookmarks: Video[] = [];
  isBookmarkOpen: boolean = false;
  @Output() selectedVideo = new EventEmitter<Video>();

  constructor() { }

  ngOnInit(): void { }

  onLineClick(video: Video) {
    this.selectedVideo.emit(video);
  }

}
