import { Component, EventEmitter, Output } from '@angular/core';
import { ApiService } from 'src/app/services/api-service.service';

@Component({
  selector: 'searchBar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.css']
})

export class SearchBarComponent {
  @Output() searchedValue = new EventEmitter<string>();

  constructor(public api: ApiService) { }

  onComplete(value: string) {
    var i = value.includes("youtube.com");
    if (i) {
      var index = value.indexOf("v=");
      if (index != -1) {
        var id = value.substring(index + 2, index + 13);
        this.searchedValue.emit(id);
      }
    }
  }
}
