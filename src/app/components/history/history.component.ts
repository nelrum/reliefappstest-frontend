import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Video } from 'src/app/class/video';
import { ApiService } from 'src/app/services/api-service.service';

@Component({
  selector: 'History',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.css']
})
export class HistoryComponent implements OnInit {

  @Input() history: Video[] = [];
  @Output() selectedVideo = new EventEmitter<Video>();

  constructor(public api: ApiService) { }

  ngOnInit(): void { }

  onLineClick(video: Video) {
    this.selectedVideo.emit(video);
  }

}
