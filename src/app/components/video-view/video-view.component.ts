import { AfterViewInit, Component, ElementRef, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Video } from 'src/app/class/video';

@Component({
  selector: 'videoView',
  templateUrl: './video-view.component.html',
  styleUrls: ['./video-view.component.css']
})

export class VideoViewComponent implements OnInit, AfterViewInit {

  @Input() video: Video = new Video();
  videoWidth: number = 500;
  videoHeight: number = 500;
  @Input() bookmarked: boolean = false;
  @Output() bookVideo = new EventEmitter<boolean>();

  apiLoaded: boolean = false;

  constructor(private el: ElementRef) { }

  ngAfterViewInit(): void {
    this.videoWidth = this.el.nativeElement.children[0].children[0].offsetWidth;
  }

  bookmarkVideo() {
    this.bookVideo.emit(true);
  }

  ngOnInit(): void {
    if (!this.apiLoaded) {
      // This code loads the IFrame Player API code asynchronously, according to the instructions at
      // https://developers.google.com/youtube/iframe_api_reference#Getting_Started
      const tag = document.createElement('script');
      tag.src = 'https://www.youtube.com/iframe_api';
      document.body.appendChild(tag);
      this.apiLoaded = true;
    }
    this.videoHeight = 650;
  }

}
