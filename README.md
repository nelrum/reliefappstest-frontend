# ReliefApps Test Frontend

## Steps

1. `npm install`

2. `ng serve --open`

## Build

`ng build --prod`

## Deployment

1. `npm install pm2 -g`

2. `pm2 serve dist/frontend/ 4200 --name "frontend"`

### (`Optional`) Delete Process

`pm2 delete frontend`
